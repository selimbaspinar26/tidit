import Navi from "./components//Navi";
import Homepage from "./components/Homepage";
import React, { Suspense, useRef } from 'react';
import { Route, Routes, BrowserRouter } from "react-router-dom";
import Footer from "./components/Footer";



function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Suspense fallback="loading">
          <Navi></Navi>

          <Routes >
            <Route path="/" element={<Homepage />} />

          </Routes></Suspense>
        <Footer></Footer>
      </BrowserRouter>
    </div>
  );
}

export default App;
