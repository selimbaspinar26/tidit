import React, { useEffect, useRef } from 'react';
import useState from 'react-usestateref'
import { useNavigate } from 'react-router-dom';
import {

    Col,
    Container, Row
} from 'reactstrap';
import "../assets/css/slider.css"
import "../assets/css/card.css"
import tidit_logo from '../assets/img/tidit_logo.png'
import tidit_logo1 from '../assets/img/bewell_logo1.png'

function Footer() {



    const [currentIndex, setCurrentIndex] = useState(0);
    const [translateX, setTranslateX] = useState(0);
    const [intervalId, setIntervalId] = useState(null);
    const sliderRef = useRef(null);
    const slideWidth = 85;
    const slideMargin = 10;

    const partnerSlides = [
        "https://www.revamp2-project.eu/images/customers/Softkinetic.png",
        "https://www.revamp2-project.eu/images/customers/Softeam.jpg",
        "https://www.revamp2-project.eu/images/customers/Softkinetic.png",
        "https://www.revamp2-project.eu/images/customers/Softeam.jpg",
        "https://www.revamp2-project.eu/images/customers/Softkinetic.png",
        "https://www.revamp2-project.eu/images/customers/Softeam.jpg",
        "https://www.revamp2-project.eu/images/customers/Softkinetic.png",
        "https://www.revamp2-project.eu/images/customers/Softeam.jpg",
        "https://www.revamp2-project.eu/images/customers/Softkinetic.png",
        "https://www.revamp2-project.eu/images/customers/Softkinetic.png",
        "https://www.revamp2-project.eu/images/customers/Softeam.jpg",
        "https://www.revamp2-project.eu/images/customers/Softkinetic.png",
        "https://www.revamp2-project.eu/images/customers/Softeam.jpg",
        "https://www.revamp2-project.eu/images/customers/Softkinetic.png",
        "https://www.revamp2-project.eu/images/customers/Softeam.jpg",
        "https://www.revamp2-project.eu/images/customers/Softkinetic.png",
    ];

    useEffect(() => {
        const id = setInterval(() => {
            setCurrentIndex((prevIndex) => prevIndex + 1);
        }, 5000);
        setIntervalId(id);

        return () => clearInterval(intervalId);
    }, []);

    useEffect(() => {
        if (currentIndex < 0) {
            setCurrentIndex(partnerSlides.length - 1);
        } else if (currentIndex >= partnerSlides.length) {
            setCurrentIndex(0);
        }
        setTranslateX(-(slideWidth + slideMargin * 2) * currentIndex);
    }, [currentIndex]);

    const handlePrevClick = () => {
        setCurrentIndex((prevIndex) => prevIndex - 1);
    };

    const handleNextClick = () => {
        setCurrentIndex((prevIndex) => prevIndex + 1);
    };



    return (
        <Container className='mb-4'>
            <Row>
                <Col>
                    <div className="partner-slider-container">
                        <div
                            className="partner-slider"
                            style={{ transform: `translateX(${translateX}px)` }}
                            ref={sliderRef}
                        >
                            {partnerSlides.map((slide, index) => (
                                <div key={index} className="partner-slide-container">
                                    <img src={slide} alt="Partners" />
                                </div>
                            ))}
                        </div>
                        <div className="slider-nav">
                            <button className="prev-button" onClick={handlePrevClick}>
                                &lt;
                            </button>
                            <button className="next-button" onClick={handleNextClick}>
                                &gt;
                            </button>
                        </div>
                    </div>
                </Col >

            </Row >

        </Container >

    );
}


export default Footer