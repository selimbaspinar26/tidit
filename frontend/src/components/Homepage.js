import React, { useEffect, useRef } from 'react';
import useState from 'react-usestateref'
import { useNavigate } from 'react-router-dom';
import {
    CardBody,
    CardHeader,
    Card,
    Col,
    Container, Row
} from 'reactstrap';
import "../assets/css/slider.css"
import "../assets/css/card.css"
import tidit_logo from '../assets/img/tidit_logo.png'
import tidit_logo1 from '../assets/img/bewell_logo1.png'

function Homepage() {

    const [slideIndex, setSlideIndex, slideindexref] = useState(1);

    useEffect(() => {
        showDivs(slideindexref.current)
    }, [])


    function plusDivs(n) {
        setSlideIndex(slideindexref.current + n)
        showDivs(slideindexref.current);
        console.log("asdsa")
    }
    function currentDiv(n) {
        setSlideIndex(n)
        showDivs(slideindexref.current);
        console.log("asdsa")

    }

    function showDivs(n) {
        var i;
        var x = document.getElementsByClassName("main-slide");
        var dots = document.getElementsByClassName("demo");
        if (n > x.length) { slideindexref.current = 1 }
        if (n < 1) { slideindexref.current = x.length }
        for (i = 0; i < x.length; i++) {
            x[i].style.display = "none";
        }
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" w3-white", "");
        }
        x[slideindexref.current - 1].style.display = "block";
        dots[slideindexref.current - 1].className += " w3-white";
    }

    let history = useNavigate();


    return (
        <Container>
            <Row>
                <Col xs="8">
                    <div className="w3-content w3-display-container" style={{ width: "800px" }}>
                        <img className="main-slide" src={tidit_logo} style={{ width: "100%" }} />
                        <img className="main-slide" src={tidit_logo1} style={{ width: "100%" }} />
                        <img className="main-slide" src={tidit_logo} style={{ width: "100%" }} />
                        <div className="w3-center w3-container w3-section w3-large w3-text-white w3-display-bottommiddle" style={{ width: "100%" }}>
                            <div className="w3-left w3-hover-text-khaki" onClick={() => plusDivs(-1)}>&#10094;</div>
                            <div className="w3-right w3-hover-text-khaki" onClick={() => plusDivs(1)}>&#10095;</div>
                            <span className="w3-badge demo w3-border w3-transparent w3-hover-white" onClick={() => currentDiv(1)}></span>
                            <span className="w3-badge demo w3-border w3-transparent w3-hover-white" onClick={() => currentDiv(2)
                            }></span >
                            <span className="w3-badge demo w3-border w3-transparent w3-hover-white" onClick={() => currentDiv(3)}></span >
                        </div >
                    </div >
                </Col >
                <Col xs="4">
                    <div className="row2-container">
                        <div className="box orange">
                            <h2>Karma</h2>
                            <p>Regularly evaluates our talent to ensure quality</p>
                        </div>
                    </div>
                </Col>
            </Row >
            <Row>
                <Col>
                    <h2 className='text-center mt-4'>About Tidit</h2>
                    <p>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                        Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a
                        galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also
                        the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the
                        release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like
                        Aldus PageMaker including versions of Lorem Ipsum.
                    </p>
                </Col>
            </Row>
            <Row>
                <Col xs="8">
                    <div>
                        <h2 className='text-center mt-4'>
                            Latest News
                        </h2>
                        <Row>
                            <Card className='mb-4'>
                                <CardHeader>
                                    <h3>Lorem Ipsum</h3>
                                </CardHeader>
                                <CardBody>
                                    <div>
                                        <img src={tidit_logo} style={{ width: "40%", float: "left", marginRight: "20px" }} />
                                        <p  >
                                            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a
                                            galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also
                                            the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the
                                            release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like
                                            Aldus PageMaker including versions of Lorem Ipsum.
                                            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a
                                            galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also
                                            the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the
                                            release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like
                                            Aldus PageMaker including versions of Lorem Ipsum.
                                        </p>
                                    </div>
                                </CardBody>
                            </Card>
                        </Row>
                        <Row>
                            <Card className='mb-4'>
                                <CardHeader>
                                    <h3>Lorem Ipsum</h3>
                                </CardHeader>
                                <CardBody>
                                    <div>
                                        <img src={tidit_logo} style={{ width: "40%", float: "left", marginRight: "20px" }} />
                                        <p  >
                                            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a
                                            galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also
                                            the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the
                                            release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like
                                            Aldus PageMaker including versions of Lorem Ipsum.
                                            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a
                                            galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also
                                            the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the
                                            release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like
                                            Aldus PageMaker including versions of Lorem Ipsum.
                                        </p>
                                    </div>
                                </CardBody>
                            </Card>
                        </Row>



                    </div>
                </Col>
                <Col xs="4">
                    <div>
                        <h2 className='text-center mt-4'>
                            Twitter
                        </h2>
                        <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a
                            galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also
                            the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the
                            release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like
                            Aldus PageMaker including versions of Lorem Ipsum.
                        </p>
                    </div>
                </Col>
            </Row>


        </Container >

    );
}


export default Homepage