import React, { useEffect, useRef } from 'react';
import { useNavigate } from 'react-router-dom';

import {
    DropdownItem,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    Button,
} from 'reactstrap';
import '../assets/css/navi.css'
import tidit_logo from '../assets/img/tidit_logo.png'

function Navi() {


    let history = useNavigate();



    const gohomepage = () => {
        history("/");
    }
    return (
        <div className="nav">
            <div className='nav-image'>
                <img src={tidit_logo}></img>
            </div>
            <ul className='nav-links'>
                <li>Home</li>
                <li>About</li>
                <li>Consortium</li>
                <li>Publications</li>
                <li>Events & News </li>
                <li>Contact Us</li>

            </ul>

        </div>


    );
}


export default Navi